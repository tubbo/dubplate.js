Vagrant.configure("2") do |config|
  # All Vagrant configuration is done here. The most common configuration
  # options are documented and commented below. For a complete reference,
  # please see the online documentation at vagrantup.com.
  config.vm.hostname = "dubplate"

  # Assign this VM to a host-only network IP, allowing you to access it
  # via the IP. Host-only networks can talk to the host machine as well as
  # any other machines on the same network, but cannot be accessed (through this
  # network interface) by any external networks.
  config.vm.network :private_network, ip: "33.33.33.10"

  # SSH configuration
  config.ssh.max_tries = 40
  config.ssh.timeout   = 120

  # Use Virtualbox to host the local development VM
  config.vm.provider :virtualbox do |provider, override|
    override.vm.box = "precise64"
    override.vm.box_url = "http://files.vagrantup.com/precise64.box"
  end

  # Use a DigitalOcean droplet in production
  config.vm.provider :digital_ocean do |provider, override|
    override.ssh.private_key_path = '~/.ssh/id_rsa'
    override.vm.box = "digital_ocean"
    override.vm.box_url = "https://github.com/smdahlen/vagrant-digitalocean/raw/master/box/digital_ocean.box"
    provider.client_id = ENV['DO_CLIENT_ID']
    provider.api_key = ENV['DO_API_KEY']
    provider.ssh_key_name = "digitalocean"
  end

  # Install the latest version of Chef
  config.omnibus.chef_version = "11.4.0"

  # Use Berkshelf to download and manage Chef cookbooks.
  config.berkshelf.enabled = true

  # Provision the server with Chef
  config.vm.provision :chef_solo do |chef|
    chef.roles_path = 'config/chef/roles'
    chef.json = {
      dubplate: {
        port: 80,
        whitelist: %w(
          psychedeli.ca
          thewonderbars.com
          waxpoeticrecords.com
        )
      }
    }
    chef.add_role 'httpd'
  end
end
