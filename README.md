# Dubplate

A simple HTTP redirection agent for your VPS. Allows one to redirect
requests for <http://example.com> to <http://www.example.com>. Useful if
you're building apps on [Heroku][heroku].

Dubplate was written in [Node.js][node] and [CoffeeScript][cs]. These
are the only hard dependencies.

## Give Me a Dubplate

The easiest way to install Dubplate, by far, is through NPM:

```bash
npm install -g coffee-script dubplate
```

In order to execute the binary, you need CoffeeScript installed
globally.

## Usage

On startup, Dubplate reads from a list of whitelisted domains in
**/etc/dubplate.json**...

```json
{
  "port": 80,
  "whitelist": [
    "example.com"
  ]
}
```

It's an array of Strings that are iterated over upon each request in
order to determine which sites are authorized to redirect.

Save this and run the following command to start up Dubplate:

```bash
$ dubplate
```

This will start the server on <http://127.0.0.1:3000> and begin serving
requests immediately. You can also pass `--port=80` to run the server on
a more reasonable port number. Running `dubplate` with no arguments
defaults to using a whitelist config at `config/whitelist.json` and
listening on port 3000.

### Automatic Install

Dubplate comes with a cookbook and Chef scripts in this repo. You can
clone down the repo on your box and run `chef-solo` to get everything
you need. Plus, you can override the generated `/etc/dubplate.json` with
your own configuration!

**Note:** Requires Ruby 1.9+

```bash
$ gem install chef
$ git clone https://github.com/tubbo/dubplate.git
$ cd dubplate
$ chef-solo -J config/yourmachine.json
```

## Development

Dubplate was written with [CoffeeScript][cs] because I like it.

Please submit your contributions along with tests and in a Git or GitHub
pull request.

## License

Just don't sue me. :)

    The MIT License (MIT)

    Copyright (c) 2013 Tom Scott

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.

[heroku]: http://heroku.com
[node]: http://nodejs.org
[npm]: http://npmjs.org
[cs]: http://coffeescript.org
