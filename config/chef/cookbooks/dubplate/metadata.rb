name             'dubplate'
maintainer       'Tom Scott'
maintainer_email 'tubbo@psychedeli.ca'
license          'All rights reserved'
description      'Installs/Configures the dubplate HTTPD'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.0.2'

