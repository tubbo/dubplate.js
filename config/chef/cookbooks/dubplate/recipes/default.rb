#
# Cookbook Name:: dubplate
# Recipe:: default
#
# Copyright (C) 2013 YOUR_NAME
# 
# All rights reserved - Do Not Redistribute
#

execute "install coffeescript from npm" do
  command "npm install -g coffee-script"
end

execute "install dubplate from npm" do
  command "npm install -g dubplate"
end

cookbook_file "/etc/init/dubplate.conf" do
  source "init.conf"
  mode '0644'
  user 'root'
  group 'root'
  action :create
end

directory "/etc/dubplate" do
  mode '0666'
  user 'root'
  group 'root'
  action :create
end

template "/etc/dubplate.json" do
  source 'dubplate.json.erb'
  mode '0666'
  user 'root'
  group 'root'
  action :create
end
