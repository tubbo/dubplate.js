Server = require './dubplate/server'
Config = require './dubplate/config'
fs = require 'fs'


class Dubplate
  constructor: (args) ->
    @args = args
    console.log "Initialized Dubplate.."
    config_file_path = @args.config || '/etc/dubplate.json'
    @config = new Config(config_file_path)

  server: ->
    @_httpServer().start @config.port()

  _httpServer: ->
    new Server(@config)

module.exports = Dubplate
