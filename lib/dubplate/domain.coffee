_ = require 'underscore'

class Domain
  constructor: (headers, whitelist) ->
    @request = headers
    @host = @request.host
    @whitelist = whitelist

  response: ->
    {
      "Location": @wwwHost() + @request.url
    }

  wwwHost: -> "http://www.#{@request.host}"

  toString: -> "<Domain #{@request.host}>"

  needsRedirect: ->
    @isTopLevel() and @inWhiteList()

  isTopLevel: -> @request.host.indexOf('www') == -1
  inWhiteList: -> _.contains @whitelist, @request.host

module.exports = Domain
