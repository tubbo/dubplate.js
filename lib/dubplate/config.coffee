fs = require 'fs'

class Config
  constructor: (path) ->
    @path = path
    throw "Error: File not found at '#{@path}'" unless @exists()

  exists: ->
    fs.existsSync @path

  port: ->
    @attributes().port

  whitelist: ->
    @attributes().whitelist

  attributes: ->
    JSON.parse @_contents()

  _contents: ->
    fs.readFileSync @path

module.exports = Config
