http = require 'http'
fs = require 'fs'
{exec} = require 'child_process'

Domain = require './domain'

class Server
  constructor: (config) ->
    @config = config
    @port = @config.port()
    @domains = @config.whitelist()
    console.log "Initialized whitelist at #{@config.path}: #{@domains}"

  start: ->
    @_getHTTPServer().listen(@port)

  _getHTTPServer: ->
    console.log "Starting server on port #{@port}"
    http.createServer (request, response) =>
      domain = new Domain(request.headers, @domains.all())
      message = switch true
        when domain.needsRedirect()
          response.writeHead 301, domain.response()
          "Redirected to #{domain.wwwHost()}"
        when not domain.isTopLevel()
          response.statusCode = 404
          "'#{domain.host}' is not a top-level domain."
        when not domain.inWhiteList()
          response.statusCode = 403
          "'#{domain.host}' is not in the white list."
        else
          response.statusCode = 500
          "'#{domain.host}' could not be redirected."

      console.log "#{response.statusCode}:", message
      response.end()

  _writePIDFile: ->
    fs.writeFile '/var/run/dubplate.pid', process.pid
    process.on 'SIGINT', @_clearPIDFile
    process.on 'SIG', @_clearPIDFile

  _clearPIDFile: ->
    console.log "Shutting down Dubplate HTTPD..."
    exec 'rm -f /var/run/dubplate.pid'

module.exports = Server
