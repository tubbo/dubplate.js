Domain = require '../../lib/dubplate/domain'

describe "Domain", ->
  describe "with a top-level URL in the whitelist", ->
    beforeEach ->
      @headers =
        host: "example.com"
        url: "/example/path"
      @domain = new Domain @headers, ["example.com"]

    it "redirects to www", ->
      expect(@domain.wwwHost()).toEqual("http://www.example.com")

    it "is top level", ->
      expect(@domain.isTopLevel()).toBeTruthy()

    it "appears in white list", ->
      expect(@domain.inWhiteList()).toBeTruthy()

    it "can be redirected", ->
      expect(@domain.needsRedirect()).toBeTruthy()

  describe "with a www url in the whitelist", ->
    beforeEach ->
      @headers =
        host: "www.example.com"
        url: "/example/path"
      @domain = new Domain @headers, ["www.example.com"]

    it "is not top level", ->
      expect(@domain.isTopLevel()).toBeFalsy()

    it "appears in white list", ->
      expect(@domain.inWhiteList()).toBeTruthy()

    it "can not be redirected", ->
      expect(@domain.needsRedirect()).toBeFalsy()

  describe "with a top-level url not in the whitelist", ->
    beforeEach ->
      @headers =
        host: "example.com"
        url: "/example/path"
      @domain = new Domain @headers, ["example.org"]

    it "is not top level", ->
      expect(@domain.isTopLevel()).toBeTruthy()

    it "appears in white list", ->
      expect(@domain.inWhiteList()).toBeFalsy()

    it "can not be redirected", ->
      expect(@domain.needsRedirect()).toBeFalsy()
