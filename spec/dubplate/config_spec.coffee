Config = require '../../lib/dubplate/config'

console.log = (x) -> null

describe "Config", ->
  beforeEach ->
    @config = new Config("spec/fixtures/dubplate.json")

  it "reads from file", ->
    expect(@config._contents()).toMatch(/\[|\]/)

  it "finds whitelist from file", ->
    expect(@config.whitelist()).toEqual(['example.com'])

  it "finds port from file", ->
    expect(@config.port()).toEqual(3000)
