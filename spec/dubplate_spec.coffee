Dubplate = require '../lib/dubplate'

describe 'Dubplate', ->
  beforeEach ->
    @runner = new Dubplate config: "spec/fixtures/dubplate.json"

  it "defaults to passed-in string as a config", ->
    expect(@runner.config.path).toEqual("spec/fixtures/dubplate.json")

  it "instantiates a dubplate server", ->
    expect(@runner._httpServer()).toBeDefined()

  it "starts the server", ->
    #TODO: expect(@runner.server()).toBeDefined()
